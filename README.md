# Poire

Poire is a prototype web controller for SATIE.

## Requirements
In order to run Poire, you'll need to install the following dependencies:

| Dependencies                                  | Instructions                                                                                                                                         |
|-----------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------|
| [SATIE](https://gitlab.com/sat-metalab/SATIE) | Follow the [Installation Guide](https://gitlab.com/sat-metalab/SATIE/-/blob/master/INSTALL-SATIE.md) of SATIE.                                       |
| [Python 3](https://www.python.org/)           | On Ubuntu 20.04, it is installed by default.                                                                                                         |
| [NodeJS v14.17.0 LTS](https://nodejs.org/en/) | On Ubuntu 20.04, you should update the default version by following [this guide](https://github.com/nodesource/distributions/blob/master/README.md). |
| [NPM](https://www.npmjs.com/package/n)        | It should be installed by default with NodeJS.                                                                                                       |

The use of Python and NodeJS for development can cause conflicts with the default versions of your system. That's why we **strongly** recommand the use of:
- [Virtualenv](https://virtualenv.pypa.io/en/latest/) that manages multiple Python environment.
- [Node Version Manager (NVM)](https://github.com/nvm-sh/nvm) that manages multiple NodeJS versions.

## Introduction
The Poire stack is composed of a *Back-End* and a *Front-End*, so you must run every parts of the stack in order to have a functionnal Poire setup.
We recommand to fullfil each part in different terminal sessions.

First, you need to clone the source that contains all the stack on your system:

```bash
git clone https://gitlab.com/sat-metalab/poire.git
cd poire
```

According to your needs and the focus of your development, you should read the dedicaded `README` for:
- The [Poire client](./client/README.md) that defines the Web Interface of Poire.
- The [Poire server](./server/README.md) that connects the Front-End with [SATIE](https://gitlab.com/sat-metalab/SATIE).

## Troubleshooting
### CI Failures
The dependencies for the `client` and the `server` are built into a `cache` and should be distributed across the CI jobs. Nevertheless, some problems have been experienced and some dependencies are not well uploaded across the CI jobs. This issue is documented [here](https://jira.sat.qc.ca/browse/HAPTIC-151) but reloading the broken pipeline should resolve the dependencies upload issue.
