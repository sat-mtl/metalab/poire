#!/usr/bin/env python3

import logging
import json
import time
import math

from typing import Any, Dict, Tuple

from pysatie.satie import Satie

from aiohttp import web
import socketio

logging.basicConfig(level=logging.WARNING)
log = logging.getLogger('poire')

satie = Satie()
app = web.Application()

sio = socketio.AsyncServer(logger=False, async_mode="aiohttp", cors_allowed_origins="*")

sio.attach(app)


@sio.event
def connect(sid: str, environ: Dict[str, Any]):
    """
    Handle a SocketIO client connection
    :param sid: ID of the client socket
    """
    version = environ["QUERY_STRING"].split("&")[0].split("=")[-1] or "Undefined"
    sio.enter_room(sid, "web-clients")
    logging.info(f"A new client is connected with version {version}")


@sio.on("/satie/initialize")
def initialize_satie(sid: str) -> Tuple[None, bool]:
    """
    Handle the SATIE initialization
    :param sid: ID of the client socket
    :return: A tuple that controls the initialization
    """
    satie.initialize()
    return None, True


@sio.on("/satie/disconnect")
def disconnect_satie(sid: str) -> Tuple[None, bool]:
    """
    Handle the SATIE disconnection
    :param sid: ID of the client socket
    :return: A tuple that controls the disconnection
    """
    satie.disconnect()
    return None, True


@sio.on("/satie/connected")
def is_satie_connected(sid: str) -> Tuple[None, bool]:
    """
    Check if SATIE is connected
    :return: A tuple that returns SATIE's connection status
    """
    logging.debug(f"The connection status of SATIE is {satie._connected}")
    return None, satie._connected


@sio.on("/satie/configure")
def configure_satie(sid: str, configuration: Any) -> Tuple[None, bool]:
    """
    Set a new configuration for SATIE
    :param sid: ID of the client socket
    :param configuration: A new configuration for SATIE
    :return: A tuple that controls the configuration
    """
    satie.configure(json.dumps(configuration))
    return None, True


@sio.on("/satie/renderer/setOutputDB")
async def set_satie_renderer_output_db(
    sid: str, output_db: float
) -> Tuple[None, float]:
    """
    Handle the change of the output DB for the SATIE renderer
    :param sid: ID of the client socket
    :param output_db: DB value of the output
    :return: A tuple that controls the change of the renderer output
    """
    satie.set_renderer_volume(float(output_db))

    await sio.emit("/satie/renderer/outputDB/changed", data=output_db, skip_id=sid)

    return None, output_db


@sio.on("/satie/audioSources/query")
async def get_plugin_list(sid: str):
    """
    Get the plugins list
    :param sid: ID of the client socket
    :return: A tuple that controls the change of the renderer output
    """
    satie.query_audiosources()
    time.sleep(0.5)

    if satie.plugins:
        plugs = [plugin.name for plugin in satie.plugins]
        await sio.emit("/satie/plugins/update", plugs)
    else:
        plugs = []

    return None, plugs


@sio.on("/satie/scene/nodes/query")
async def get_nodes(sid: str):
    pass


def get_plugin_properties(plug_name):
    return [
        [(prop.name, prop.type, prop.default_value) for prop in plugin.properties]
        for plugin in satie.plugins
        if plugin.name == plug_name
    ]


@sio.on("/satie/scene/createSource")
async def create_source_node(sid: str, node: Any):
    """
    Create a node
    :param sid: ID of the client socket
    :param node: a new node to add
    :return: A tuple that controls the change of the renderer output
    """
    plugin_ = satie.get_plugin_by_name(node["synthdefName"])
    satie.create_source_node(node["nodeName"], plugin_, group="default")
    properties_ = get_plugin_properties(node["synthdefName"])
    print(properties_)
    await sio.emit(
        "/satie/plugins/confirmationCreation",
        {"name": node["nodeName"], "node": node, "proprieties": properties_[0]}
    )

    return None, True


@sio.on("/satie/scene/updateSource")
def update_source_node(sid: str, node: Any):
    """
    Update basic parameters of a source node
    :param sid: ID of the client socket
    :param node: the node to update
    :return: A tuple that controls the change of the renderer output
    """
    satie.node_update(
        node["nodeName"],
        node["azimuth"],
        node["elevation"],
        node["gain"],
        node["delay"],
        node["lphz"],
    )

    return None, True


@sio.on("/satie/scene/updateSynthdefSource")
def set_source_node(sid: str, node: Any):
    """
    update a synthdef parameters of a source node
    :param sid: ID of the client socket
    :param node: the node to update and the parameters to update
    :return: A tuple that controls the change of the renderer output
    """
    satie.node_set(
        node["nodeName"],
        node["param"]["propertie"],
        node["param"]["value"]
    )

    return None, True


@sio.on("/satie/scene/deleteSource")
def delete_node(sid: str, node: Any):
    """
    delete a source node
    :param sid: ID of the client socket
    :param node: the source node to delete
    :return: A tuple that controls the change of the renderer output
    """
    satie.delete_node(node)

    return None, True


@sio.on('/satie/tester/testDacChannel')
def test_satie_dac_channel(sid: str, dac_channel: int, synth_def: str, extra_args: Any) -> Tuple[None, int]:
    """
    Handle the test of a DAC channel
    :param sid: ID of the client socket
    :param dac_channel: Number of the DAC channel to test
    :param synth_def: String of the test synth
    :return: A tuple that controls the test
    """
    log.debug(f'Test channel {dac_channel}',
              extra={'synth_def': synth_def, 'extra_args': extra_args})

    synth_args=[item for k in extra_args for item in (k, extra_args[k])]
    satie.dac_test_channel(int(dac_channel), synth_def, *synth_args)

    return None, dac_channel


@sio.on('/satie/tester/stopDacChannel')
def stop_satie_dac_channel(sid: str, dac_channel: int) -> Tuple[None, int]:
    """
    Stop of the test of a DAC channel
    :param sid: ID of the client socket
    :param dac_channel: Number of the tested DAC channel
    :return: A tuple that controls the test
    """
    satie.stop_test_channel(int(dac_channel))
    return None, dac_channel


if __name__ == "__main__":
    web.run_app(app, port="5000")
