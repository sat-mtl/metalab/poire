# Poire Back-End
The poire back-end is a bridge between [pySATIE](https://gitlab.com/sat-metalab/PySATIE) and the poire client by using the [WebSocket Protocol](https://datatracker.ietf.org/doc/html/rfc6455). It handles the connection of multiple clients with a system running a [SATIE](https://gitlab.com/sat-metalab/SATIE) instance.

It uses [python-socketio](https://github.com/miguelgrinberg/python-socketio) in order to implement this connection.

## Setup the project
You should execute all instructions that starts the Back-End from the `server` folder.

```bash
cd server
```


### Activate the Python environment
When you have [Virtualenv](https://virtualenv.pypa.io/en/latest/) installed, you should create a new Python environment by doing:

```bash
[[ ! -d "./venv" ]] && python3 -m venv venv
. venv/bin/activate
```

### Install the server dependencies
Now, you should be able to install the dependencies by using the `requirements.txt` file:

```bash
pip install -r requirements.txt
```

## Getting Started
### Start SATIE from SuperCollider
Before you can do anything, you need to have SATIE up and running on your system.
A very basic SATIE project will do the trick. Run the following inside the SuperCollider IDE:

```supercollider
// boot SATIE
(
a = Satie.new;
a.debug = true;
a.boot;
)

// when finished testing
a.quit;
```

### Launch the server
Enventually, you can launch the server by executing the script `bridge.py`:

```bash
python bridge.py
```

## Troubleshooting
The client and the server use the [Socket.IO](https://socket.io/) stack and need to communicate with the same version of the protocol. Some updates could imply some incompatibilities and you can consult the compatibility table [here](https://python-socketio.readthedocs.io/en/latest/intro.html#version-compatibility).
