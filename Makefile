DIST_DIR := client/dist
WWW_DIR := /var/www/poire
LOG_DIR := /var/log/poire
H2O_ETC := /etc/h2o/h2o.conf

.PHONY: build
build:
	$(info Build the client)
	cd client && npm ci
	cd client && NODE_OPTIONS="--max_old_space_size=4096" npm run build

.PHONY: configure-h2o
configure-h2o:
	$(info Configure the client for the H2O server)
	mkdir -p $(LOG_DIR)
	cp config/h2o.yml $(H2O_ETC)
	systemctl restart h2o

.PHONY: install-client
install-client: configure-h2o
	$(info Install the client distribution on localhost:80)
	mkdir -p $(WWW_DIR)
	cp -r $(DIST_DIR)/* $(WWW_DIR)

.PHONY: install
install: install-client

.PHONY: uninstall
uninstall-client:
	$(info Uninstall the client distribution)
	-rm -rd $(WWW_DIR)
	-rm $(H2O_ETC)

.PHONY: uninstall
uninstall: uninstall-client

.PHONY: clean
clean:
	$(info Clean the client and the server directories)
	rm -rdf $(DIST_DIR)
	rm -rdf client/node_modules
	rm -rdf server/venv
