#!/bin/bash
################################################################################################
#                                   GIT PRECOMMIT HOOK                                         #
# This script checks if the formatting of .py files is correct and                             #
# proposes to fix it automatically, let the user do it or ignore the formatting (don't do it!).#
################################################################################################

##################################
# Global variables
AUTOPEP8="autopep8"
MYPY="mypy"
MYPYPATH=""
files_unformatted=""
files_type_error=""
python_files=()
line_length=120
###################################

##################################
# Helpers

reformat_file() {
  while true
  do
    read -p "Do you want to do the reformatting (M)anually or let it be done (A)utomatically? [m/A]" reply < /dev/tty
    case $reply in
      [Mm]) echo -e "Commit will be aborted, awaiting reformatting of \e[0;32m${file}\e[0m."; files_unformatted+="\t\e[0;32m$1\e[0m\n"; return;;
      "") ;& # Fallthrough default behaviour, the reformatting is automatic.
      [Aa]) echo -e "Automatic reformatting of \e[0;32m${file}\e[0m will be done now."; ${AUTOPEP8} -i --max-line-length ${line_length} $1; git add $1; return;;
      *) echo "Unrecognized option." ;;
    esac
  done
}

final_check() {
  echo -e "\n\nThe following files are not correctly formatted:"
  echo -e $files_unformatted

  while true
  do
    read -p "Do you want to (I)gnore the formatting and commit anyway (not recommended) or do the reformatting (Y)ourself ? [i/Y]" reply < /dev/tty
    case $reply in
      [Ii]) echo "Ignoring the formatting and committing files as-is."; exit 0;;
      "") ;& # Fallthrough default behaviour, the commit is aborted.
      [Yy]) echo "Commit aborted, awaiting reformatting of the previously listed files."; exit 1;;
      *) echo "Unrecognized option." ;;
    esac
  done
}

check_command() {
  if ! type $1 > /dev/null
  then
    echo "Error: $1 executable not found."
    exit 1
  fi
}

lint_python_codebase () {
  local python_files=${1}

  # Nothing to test if no file was added, changed or modified.
  [ -z "$python_files" ] && exit 0

  for file in ${python_files}
  do
    output=$(echo $(${AUTOPEP8} --diff --max-line-length ${line_length} ${file} | wc -l))
    if [ "${output}" -ne 0 ]
    then
      echo -e "\e[0;31m${file} does not conform to coding standards, here are the changes needed:\n\n\e[0m$(${AUTOPEP8} --diff --max-line-length ${line_length} ${file})"
      reformat_file $file
    fi
  done
}

check_python_types () {
  local python_files=${1}

  has_type_error=0
  for file in ${python_files}
  do
    output=$(MYPYPATH="${MYPYPATH}" mypy --incremental --follow-import silent --ignore-missing-imports --no-error-summary "${file}")
    if [ ! -z "${output}" ]
    then
      echo
      echo -e "File \e[0;32m${file}\e[0m contains type errors:"
      echo "${output}"
      has_type_error=1
    fi
  done

  if ["${has_type_error}" == 1 ]
  then
    echo ""
    read -p "Errors were found in the committed files, do you want to (I)gnore them and commit anyway, or (M) post-pone this commit and fix them? [i/M]" reply < /dev/tty
    case ${reply} in
      [Mm]) echo "Aborting commit to allow for fixing them."; exit 1;;
      "") echo "Aborting commit to allow for fixing them."; exit 1;; # Fallthrough default behaviour, the commit is aborted.
      [Ii]) echo "Ignoring the type check issues and committing files as-is."; exit 0;;
      *) echo "Unrecognized option."; exit 1;;
    esac
  fi
}

lint_javascript_codebase () {
  cd client
  npx standard
}

##################################

##################################
# Main
python_files=$(git diff --cached --name-only --diff-filter=ACM | grep '\.py')

##
## Formatting
check_command ${AUTOPEP8}
lint_python_codebase ${python_files}


# If any Python file is not formatted properly, do it now
[ ! -z "$files_unformatted" ] && final_check

##
## Type checking
check_command ${MYPY}
check_python_types ${python_files}

lint_javascript_codebase
##################################
