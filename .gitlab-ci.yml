cache: # Should be stored in a S3 bucket
  key:
    files:
      - client/package-lock.json
    prefix: ${CI_COMMIT_REF_SLUG}
  paths:
    - client/node_modules/

stages:
  - setup
  - configure
  - build
  - test
  - release

# Setup all the client dependencies
setup-client:
  image: node:lts
  stage: setup
  before_script:
    - cd client
  script:
    - npm ci
  artifacts: # Fallback for cache, should be removed if the cache is configured!
    expire_in: 1 day
    paths:
      - client/node_modules/

# Setup all the server dependencies
setup-server:
  image: python:buster
  stage: setup
  before_script:
    - cd server
  script:
    - pip install wheel pyliblo3
    - pip install -r requirements.txt

# Lint the client codebase with the StandardJS linter
lint-client:
  image: node:lts
  stage: configure
  before_script:
    - cd client
  script:
    - npm run lint

# Build the client into the dist folder
build-client:
  image: node:lts
  stage: build
  variables:
    NODE_OPTIONS: "--max_old_space_size=4096"
  before_script:
    - cd client
  script:
    - npm run build
  artifacts:
    paths:
      - client/dist
    expire_in: 1 day

# Test all client unit tests with their coverage
test-client:
  image: node:lts
  stage: test
  before_script:
    - cd client
  script:
    - npm run test:coverage
  artifacts:
    name: "unit-test-coverage-${CI_COMMIT_REF_NAME}"
    paths: ['coverage']

# Bundle and release production build of the webapp to
# https://gitlab.com/sat-metalab/poire/-/jobs/artifacts/<version>/raw/poire-<version>.tar.gz?job=release-client-bundle
# WARNING: This job will not work on branch that includes slashes!
release-client-bundle:
  stage: release
  variables:
    BUNDLE_NAME: "${CI_PROJECT_NAME}-${CI_COMMIT_REF_NAME}"
  script:
    - tar -zcvf "${BUNDLE_NAME}.tar.gz" -C client/dist .
  artifacts:
    name: "${BUNDLE_NAME}"
    paths: ["${BUNDLE_NAME}.tar.gz"]
    expire_in: never
  only: ['develop', 'master']

# Deploy Poire on development S3 bucket
# WARNING: The branch where this job is executed should be protected
deploy-development-bucket:
  image: python:3.8-buster
  stage: release
  before_script:
    - pip3 install awscli
  script:
    - if [[ -z "${AWS_ACCESS_KEY_ID}" || -z "${AWS_SECRET_ACCESS_KEY}" ]]; then echo "AWS credentials are not configured!"; exit 1; fi
    - aws s3 sync client/dist/ s3://poire-web-develop/ --delete
  only: ['develop']

# Deploy Poire on production S3 bucket
# WARNING: The branch where this job is executed should be protected
deploy-production-bucket:
  image: python:3.8-buster
  stage: release
  before_script:
    - pip3 install awscli
  script:
    - if [[ -z "${AWS_ACCESS_KEY_ID}" || -z "${AWS_SECRET_ACCESS_KEY}" ]]; then echo "AWS credentials are not configured!"; exit 1; fi
    - aws s3 sync client/dist/ s3://poire-web/ --delete
  only: ['master']
