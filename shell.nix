with (import <nixpkgs> {});

# This `nix-shell` script will install all the Poire stack in an isolated environment
# It will configure NodeJS, Python with all required Poire tools without intalling them on your system
let
  pyPkgs = python39.withPackages (packages: with packages; [
    pip
    python-socketio
    aiohttp
  ]);

  pyliblo3 = python39Packages.buildPythonPackage rec {
    pname = "pyliblo3";
    version = "0.12.0";

    src = python39Packages.fetchPypi {
      inherit pname version;
      sha256 = "1bwqarbsza5nf73rclmxy24wks83byrd1rk3m28dkgrdnyp19xn0";
    };

    propagatedBuildInputs = [
      liblo
      python39Packages.cython
    ];
  };

  pySatie = python39Packages.buildPythonPackage {
    name = "pySatie";

    src = builtins.fetchGit {
      url = "https://gitlab.com/sat-metalab/PySATIE.git";
      ref = "develop";
    };

    propagatedBuildInputs = with python39Packages; [
      cython
      pyliblo3
      blinker
      psutil
    ];
  };
in mkShell {
  name = "poire";

  buildInputs = [
    pyPkgs
    pySatie
    nodejs-14_x nodePackages.npm-check-updates # js tools
    entr
    awscli2
  ];
}
