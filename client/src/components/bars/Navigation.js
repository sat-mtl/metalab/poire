import React, { useContext } from 'react'
import { observer } from 'mobx-react-lite'
import classNames from 'classnames'

import PageEnum from '@models/PageEnum'
import { AppStoresContext } from '@components/App'

import SettingsIcon from '@assets/svg/power_on.svg'
import PlaygroundIcon from '@assets/svg/headset_24px.svg'

import { Common } from '@sat-valorisation/ui-components'

import '@styles/bars/Navigation.scss'
const { Icon } = Common

/**
 * A tab for the navigation
 * @memberof module:components/bars.Navigation
 * @selector `.NavTab`
 * @param {external:react/Component} children - The content of the Tab
 * @returns {external:react/Component} A tab for the navigation
 */
const NavTab = observer(({ children, pageId }) => {
  const { pageStore } = useContext(AppStoresContext)

  const navTabClass = classNames(
    'NavTab',
    { 'NavTab-selected': pageStore.activePage === pageId }
  )

  return (
    <li
      className={navTabClass}
      onClick={() => pageStore.setActivePage(pageId)}
    >
      {children}
    </li>
  )
})

/**
 * An icon for the tabs of the navigation panel
 * @memberof module:components/bars.Navigation
 * @selector `.NavIcon`
 * @param {external:react/Component} children - Some icon as an svg or a png
 * @returns {external:react/Component} A wrapped icon
 */
function NavIcon ({ children }) {
  return (
    <div className='NavIcon'>
      {children}
    </div>
  )
}

/**
 * The navigation panel
 * @memberof module:components/bars
 * @selector `#Navigation`
 * @returns {external:react/Component} The navigation panel
 */
const Navigation = observer(() => {
  return (
    <ul id='Navigation'>
      <NavTab pageId={PageEnum.SETTINGS}>
        <NavIcon>
          <SettingsIcon />
        </NavIcon>
        <h1>
          Settings
        </h1>
      </NavTab>
      <NavTab pageId={PageEnum.DAC_TESTER}>
        <NavIcon>
          <Icon type='audio' />
        </NavIcon>
        <h1>
          DAC Tester
        </h1>
      </NavTab>
      <NavTab pageId={PageEnum.PLAYGROUND}>
        <NavIcon>
          <PlaygroundIcon />
        </NavIcon>
        <h1>
          Playground
        </h1>
      </NavTab>
    </ul>
  )
})

export default Navigation
