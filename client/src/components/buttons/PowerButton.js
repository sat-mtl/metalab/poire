import React, { useContext } from 'react'
import { observer } from 'mobx-react-lite'
import classNames from 'classnames'

import { AppStoresContext } from '@components/App'

import { Common } from '@sat-valorisation/ui-components'

import '@styles/buttons/PowerButton.scss'
const { Button, Icon } = Common

const PowerButton = observer(() => {
  const { statusStore } = useContext(AppStoresContext)
  const buttonClass = classNames(
    'PowerButton',
    { 'PowerButton-connected': statusStore.connected }
  )

  return (
    <Button
      className={buttonClass}
      onClick={() => {
        if (!statusStore.connected) {
          statusStore.applyInitialization()
        } else {
          statusStore.applyDisconnection()
        }
      }}
    >
      <div className='PowerIcon'>
        <Icon type='filled-power' color='white' />
      </div>
      {!statusStore.connected ? 'BOOT' : 'STOP'}
      &nbsp;SATIE
    </Button>
  )
})

export default PowerButton
