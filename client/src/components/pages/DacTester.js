import React, { useContext } from 'react'
import { observer } from 'mobx-react-lite'

import { AppStoresContext } from '@components/App'
import ChannelButton from '@components/buttons/ChannelButton'

import {
  TestSynthSelect,
  TestIntervalInput,
  FrequencyInput
} from '@components/fields/TesterFields'

import { Common } from '@sat-valorisation/ui-components'

import '@styles/pages/DacTester.scss'
const { Button } = Common

/**
 * A panel that displays all testable outputs
 * @returns {external:react/Component} A panel
 */
const DACTester = observer(() => {
  const { configStore, testerStore } = useContext(AppStoresContext)

  return (
    <div id='DACTester'>
      <header>
        <h1>
          DAC Tester
        </h1>
      </header>
      <main>
        <section role='form' id='TesterSettings'>
          <header>
            <h2>
              Tester Settings
            </h2>
          </header>
          <div id='TesterSettingsPanel'>
            <section id='CommonSettingsSection'>
              <h3>
                Common Settings
              </h3>
              <TestSynthSelect />
              <TestIntervalInput />
            </section>
            {testerStore.hasExtraConfig && (
              <section id='ExtraSettingsSection'>
                <h3>
                  Extra Settings
                </h3>
                {testerStore.dacConfig.testSynth === 'sine' && (
                  <FrequencyInput />
                )}
              </section>
            )}
          </div>
          <footer>
            <Button
              onClick={() => testerStore.applyDacCycle(configStore.outputChannels)}
            >
              Loop tests
            </Button>
          </footer>
        </section>
        <section id='OutputChannels'>
          <h2>
            Output Channels
          </h2>
          <div id='OutputChannelsPanel'>
            {configStore.outputChannels.map((channel) => (
              <ChannelButton
                key={channel}
                channelNum={channel}
              />
            ))}
          </div>
        </section>
      </main>
    </div>
  )
})

export default DACTester
