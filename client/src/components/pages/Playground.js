import React from 'react'
import CreateNode from '@components/panels/CreateNode'
import NodeList from '@components/panels/NodeList'
import EditNode from '@components/panels/EditNode'

import 'typeface-red-rose'

import '@styles/pages/Playground.scss'

/**
 * The playground page
 * @selector `#Playground`
 * @memberof module:components/pages
 * @returns {external:react/Component} A page
 */
function Playground () {
  return (
    <div id='Playground'>
      <div className='sources'>
        <CreateNode />
        <NodeList />
      </div>
      <EditNode />
    </div>
  )
}

export default Playground
