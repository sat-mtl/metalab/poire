import React, { useContext } from 'react'
import { observer } from 'mobx-react-lite'

import { AppStoresContext } from '@components/App'

import { Inputs } from '@sat-valorisation/ui-components'
const { Field, InputText, Number, Select, Slider } = Inputs

const SourceNodeNameInput = observer(({ value, setValue, isDisabled, status = '', description = '' }) => {
  return (
    <Field
      disabled={isDisabled}
      title='Source Name'
      status={status}
      description={description}
    >
      <InputText
        value={value}
        placeholder='Input a name'
        onChange={(e) => setValue({
          name: 'nodeName',
          value: e.target.value
        })}
        onPressEnter={setValue}

      />
    </Field>
  )
})

const SynthDefSelect = observer(({ value, setValue, isDisabled, pluginList = [] }) => {
  const getSelectedValue = (value) => {
    if (!isDisabled) {
      // return sourceStore.selectedSource[propertie]
      return pluginList.find(format => {
        return format.value === value
      })
    } else {
      return {
        label: 'default',
        value: 'default'
      }
    }
  }
  return (
    <Field
      title='SynthDefName'
        // description='Only one format at once is supported'
      disabled={isDisabled}
    >
      <Select
        options={pluginList}
        selected={getSelectedValue(value)}
        onSelection={(label) => {
          setValue({ name: 'synthdefName', value: label })
        }}
      />
    </Field>
  )
})

const NodeParamSlider = observer(({ label, name, value = '', setValue, min, max, step, isDisabled }) => {
  const { sourceStore } = useContext(AppStoresContext)
  const isSelectedNodeDefined = () => {
    return sourceStore.selectedSource === null
  }

  const common = {
    value: value,
    min: min,
    max: max,
    step: step,
    onChange: (newValue) => {
      setValue({ name: name, value: newValue })
    },
    disabled: isDisabled
  }

  const gridLayout = {
    width: '100%',
    display: 'inline-grid',
    gridTemplateColumns: '[slider] auto [input] 180px',
    gridColumnGap: '8px'
  }

  return (
    <Field
      disabled={isSelectedNodeDefined()}
      title={label}
    >
      <div style={{ ...gridLayout }}>
        <span style={{ gridArea: 'slider' }}>
          <Slider {...common} />
        </span>
        <span style={{ gridArea: 'input' }}>
          <Number {...common} />
        </span>
      </div>
    </Field>
  )
})

const BasicNumberInput = observer(({ name, value, setValue, step, isDisabled }) => {
  const common = {
    value: value,
    step: step,
    onChange: (newValue) => {
      setValue({ name: name, value: newValue })
    },
    disabled: isDisabled
  }
  return (
    <Field
      disabled={isDisabled}
      title={name}
    >
      <Number {...common} />
    </Field>
  )
})

export {
  SourceNodeNameInput,
  SynthDefSelect,
  NodeParamSlider,
  BasicNumberInput
}
