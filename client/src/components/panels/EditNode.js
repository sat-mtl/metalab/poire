import React, { useContext } from 'react'
import { AppStoresContext } from '@components/App'
import { observer } from 'mobx-react-lite'
import '@styles/common/SourceNode.scss'

import {
  SourceNodeNameInput,
  SynthDefSelect,
  NodeParamSlider,
  BasicNumberInput
} from '@components/fields/PlaygroundFields'

const EditNode = observer(() => {
  const { sourceStore, pluginStore } = useContext(AppStoresContext)

  const handleChange = (e) => {
    sourceStore.setSelectedSourceParameter(e.name, e.value)

    // if (e.name === 'synthdefName') {
    //   nodeAPI.createSource(sourceStore.selectedSource)
    // } else {
    //   nodeAPI.updateSource(sourceStore.selectedSource)
    // }
  }

  const handleSynthDefChange = (e) => {
    sourceStore.setSelectedSourceSynthdefParameter(e.name, e.value)
    // nodeAPI.updateSynthdefSource({ nodeName: sourceStore.selectedNodeName, param: { propertie: e.name, value: e.value } })
  }

  const isSelectedNodeDefined = () => {
    return sourceStore.selectedNodeName !== null
  }

  const getSourceValue = (propertie) => {
    return sourceStore.parameters.get(sourceStore.selectedNodeName)?.get(propertie)
  }

  const renderSynthDefProperties = () => {
    if (isSelectedNodeDefined()) {
      return Array.from(sourceStore.parameters.get(sourceStore.selectedNodeName)?.get('properties'), ([key, value]) => {
        const step = value.type === 'Float' ? 0.1 : 1
        return (
          <BasicNumberInput
            key={key}
            name={key}
            value={sourceStore.parameters.get(sourceStore.selectedNodeName)?.get('properties').get(key).value}
            setValue={handleSynthDefChange}
            step={step}
            isDisabled={!isSelectedNodeDefined()}
          />
        )
      })
    }
  }
  return (
    <div className='EditNode'>
      <div className='tab'>
        <h1 className='tab--text'>Edit Source Node</h1>
      </div>
      <div className='list-container'>

        <SourceNodeNameInput
          isDisabled
          value={sourceStore.selectedNodeName || undefined}
          setValue={handleChange}
        />
        <SynthDefSelect
          pluginList={pluginStore.pluginList}
          value={getSourceValue('synthdefName')}
          setValue={handleChange}
          isDisabled={!isSelectedNodeDefined()}
        />

        <NodeParamSlider
          label='Azimuth (deg)'
          name='azimuth'
          value={getSourceValue('azimuth')}
          setValue={handleChange}
          min={-180}
          max={180}
          step={1}
          isDisabled={!isSelectedNodeDefined()}
        />
        <NodeParamSlider
          label='Elevation (deg)'
          name='elevation'
          value={getSourceValue('elevation')}
          setValue={handleChange}
          min={-90}
          max={90}
          step={1}
          isDisabled={!isSelectedNodeDefined()}
        />
        <NodeParamSlider
          label='Gain (dB)'
          name='gain'
          value={getSourceValue('gain')}
          setValue={handleChange}
          min={-99}
          max={6}
          step={1}
          isDisabled={!isSelectedNodeDefined()}
        />
        <NodeParamSlider
          label='Delay (ms)'
          name='delay'
          value={getSourceValue('delay')}
          setValue={handleChange}
          min={0}
          max={1000}
          step={1}
          isDisabled={!isSelectedNodeDefined()}
        />
        <NodeParamSlider
          label='Low-pass Filter (Hz)'
          name='lphz'
          value={getSourceValue('lphz')}
          setValue={handleChange}
          min={0}
          max={15000}
          step={10}
          isDisabled={!isSelectedNodeDefined()}
        />
        <div className='listed-items'>
          <h2>SynthDef parameters</h2>
          {renderSynthDefProperties()}
        </div>
      </div>
    </div>
  )
})

export default EditNode
