import React, { useContext, useEffect } from 'react'
import { AppStoresContext } from '@components/App'
import { observer } from 'mobx-react-lite'
import '@styles/common/SourceNode.scss'
// import CloseIcon from '@assets/svg/close_icon.svg'
// import { entries } from 'mobx'
import { nodeAPI } from '@stores'
import { Feedback } from '@sat-valorisation/ui-components'
const { Notification } = Feedback

const NodeList = observer(() => {
  const { sourceStore } = useContext(AppStoresContext)
  const handleDelete = (sourceKey) => {
    sourceStore.deleteSource(sourceKey)
    nodeAPI.deleteSource(sourceKey)
  }

  useEffect(() => {
    nodeAPI.querySourceNodes()
  }, [])

  const renderSources = () => {
    return Array.from(sourceStore.parameters, ([key, value]) => {
      return (
        <Notification
          key={key}
          message={key}
          description={value.get('synthdefName')}
          onClick={() => sourceStore.setSelectedSource(key)}
          onClose={() => handleDelete(key)}
          status='active'
        />
      )
    })
  }

  // const renderSources = () => {
  //   return Array.from(sourceStore.parameters, ([key, value]) => {
  //     return (
  //       <div className='item' key={key}>
  //         <div onClick={() => sourceStore.setSelectedSource(key)} className='case'>{key}</div>
  //         <div onClick={() => sourceStore.setSelectedSource(key)} className='case'>{value.get('synthdefName')}</div>
  //         <div className='case-icon'>
  //           <CloseIcon
  //             className='close-icon'
  //             visibility='visible'
  //             onClick={() => handleDelete(key)}
  //           />
  //         </div>
  //       </div>
  //     )
  //   })
  // }

  if (sourceStore.sources) {
    return (
      <div className='NodeList'>
        <div className='tab'>
          <h1 className='tab--text'>Source Node List</h1>
        </div>
        <div className='list-container'>
          <div className='listed-items'>
            {renderSources()}
          </div>
        </div>
      </div>
    )
  }
})

export default NodeList
