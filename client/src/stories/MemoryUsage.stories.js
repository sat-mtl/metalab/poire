import React from 'react'
import MemoryUsage from '../components/Sidebar/MemoryUsage'

export default {
  title: 'Sidebar/MemoryUsage',
  component: MemoryUsage
}

export const MemoryStatic = () => <MemoryUsage />
