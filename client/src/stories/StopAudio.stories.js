import React from 'react'
import StopAudio from '../components/Sidebar/StopAudio'

export default {
  title: 'Sidebar/StopAudio',
  component: StopAudio
}

export const StopAudioButton = () => <StopAudio />
