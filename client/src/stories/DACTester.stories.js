import React from 'react'
import DACTester from '../components/DACTester/index'

export default {
  title: 'DACTester',
  component: DACTester
}

export const DACTesterDefault = () => <DACTester />
