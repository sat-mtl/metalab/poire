import React from 'react'
import NavTab from '../components/Sidebar/NavTab'

import SettingsIcon from '../assets/power_on.svg'

export default {
  title: 'Sidebar/NavTab',
  component: NavTab
}

export const SettingsTab = () => <NavTab icon={SettingsIcon} text='Settings' />

export const SettingsTabSelected = () => (
  <NavTab icon={SettingsIcon} text='Settings' selected />
)
