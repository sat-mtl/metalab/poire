/* eslint-disable react/jsx-props-no-spreading */
import React from 'react'
import Parameters from '../components/common/Parameters'

export default {
  title: 'Settings/Parameters',
  component: Parameters
}

export const ParametersStatic = (args) => <Parameters {...args} />

ParametersStatic.args = {
  label: 'SC Server',
  type: 'text',
  value: 'Supernova'
}

export const ParametersEditableSelect = (args) => <Parameters {...args} />

ParametersEditableSelect.args = {
  label: 'SC Server',
  type: 'select',
  isEditable: true,
  options: ['Supernova', 'scsynth']
}

export const ParametersEditableText = (args) => <Parameters {...args} />

ParametersEditableText.args = {
  label: 'IP Address',
  type: 'text',
  isEditable: true
}
