import React from 'react'
import OutChannelItem from '../components/DACTester/OutChannelItem'

export default {
  title: 'DACTester/OutChannelItem',
  component: OutChannelItem
}

export const OutChannelItemDefault = () => <OutChannelItem />
