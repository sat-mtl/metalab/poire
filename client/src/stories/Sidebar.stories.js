import React from 'react'
import Sidebar from '../components/Sidebar/index'

export default {
  title: 'Sidebar',
  component: Sidebar
}

export const SidebarOn = () => <Sidebar />
