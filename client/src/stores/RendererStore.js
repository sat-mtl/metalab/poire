import { makeObservable, observable, action, reaction } from 'mobx'
import RendererAPI from '@api/RendererAPI'
import logger from '@utils/logger'

/**
 * @constant {external:pino/logger} LOG - Dedicated logger for the RendererStore
 * @private
 */
export const LOG = logger.child({ store: 'RendererStore' })

/** Store all rendered values */
class RendererStore {
  /** @property {api.RendererApi} rendererApi - Create requests for the SATIE renderer */
  rendererAPI = null

  /** @property {number} [outputDB=0] - The current output DB value */
  outputDB = 0

  constructor (socketStore) {
    makeObservable(this, {
      outputDB: observable,
      setOutputDB: action
    })

    reaction(
      () => socketStore.activeSocket,
      socket => this.handleSocketChange(socket)
    )
  }

  /**
   * Handles changes to the app's socket
   * @param {external:socketIO/Socket} socket - Event-driven socket
   */
  handleSocketChange (socket) {
    this.rendererAPI = null

    if (socket) {
      this.rendererAPI = new RendererAPI(socket)

      this.rendererAPI.onOutputDBChanged(
        outputDB => this.setOutputDB(outputDB)
      )
    }
  }

  /**
   * Requests the new outputDB to the Poire server
   * WARNING: Avoid infinite loop by re-applying the value on broadcast
   * @param {number} outputDB - The new output DB value
   */
  applyOutputDB (outputDB) {
    this.rendererAPI.setOutputDB(outputDB)
    this.setOutputDB(outputDB)
  }

  /**
   * Sets a new output DB value
   * @param {number} outputDB - The new output DB value
   */
  setOutputDB (outputDB) {
    this.outputDB = outputDB

    LOG.debug({
      msg: 'The outputDB has changed',
      outputDB: outputDB
    })
  }
}

export default RendererStore
