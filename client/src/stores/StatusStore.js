import { makeObservable, observable, action, reaction } from 'mobx'
import SatieAPI from '@api/SatieAPI'
import logger from '@utils/logger'

/**
 * @constant {external:pino/logger} LOG - Dedicated logger for the StatusStore
 * @private
 */
export const LOG = logger.child({ store: 'StatusStore' })

/** Stores all statuses of SATIE */
class StatusStore {
  /** @property {api.SatieApi} satieAPI - Create requests for the SATIE lifecycle API */
  satieAPI = null

  /** @property {boolean} [connected=false] - The connection status of SATIE */
  connected = false

  /** @property {boolean} [editable=true] - The editable statue of Poire */
  editable = true

  constructor (socketStore) {
    makeObservable(this, {
      connected: observable,
      setConnectionStatus: action
    })

    reaction(
      () => socketStore.activeSocket,
      socket => this.handleSocketChange(socket)
    )
  }

  /**
   * Handles changes to the app's socket
   * @param {external:socketIO/Socket} socket - Event-driven socket
   */
  handleSocketChange (socket) {
    this.satieAPI = null

    if (socket) {
      this.satieAPI = new SatieAPI(socket)
      this.applyConnectionCheck()
    } else {
      this.setConnectionStatus(false)
    }
  }

  /**
   * Applies the SATIE initialization
   * @async
   */
  async applyInitialization () {
    try {
      await this.satieAPI.initialize()
    } catch (error) {
      LOG.error({
        msg: 'SATIE initialization failed',
        error: error
      })
    }

    this.applyConnectionCheck()
  }

  /**
   * Applies a check of the connection status of SATIE
   * @async
   */
  async applyConnectionCheck () {
    try {
      const connectionStatus = await this.satieAPI.isConnected()
      this.setConnectionStatus(connectionStatus)

      LOG.info({
        msg: 'The connection status has changed',
        connected: connectionStatus
      })
    } catch (error) {
      LOG.error({
        msg: 'The connection check has failed',
        error: error
      })
    }
  }

  /**
   * Applies the SATIE disconnection
   * @async
   */
  async applyDisconnection () {
    try {
      await this.satieAPI.disconnect()
    } catch (error) {
      LOG.error({
        msg: 'SATIE disconnection failed',
        error: error
      })
    }

    this.applyConnectionCheck()
  }

  /**
   * Set the new connection status
   * @param {boolean} connectionStatus - The new connection status
   */
  setConnectionStatus (connectionStatus) {
    this.connected = connectionStatus
  }
}

export default StatusStore
