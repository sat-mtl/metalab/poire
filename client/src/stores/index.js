/* global location */

import React, { createContext, useContext, useReducer } from 'react'
import PropTypes from 'prop-types'
import io from 'socket.io-client'

import SatieAPI from '@api/SatieAPI'
import TestAPI from '@api/TestAPI'
import RendererAPI from '@api/RendererAPI'
import NodeAPI from '@api/NodeAPI'
import PluginAPI from '@api/PluginAPI'

import logger from '@utils/logger'

/**
 * @constant {external:pino/logger} LOG - Dedicated logger for the default store
 * @memberof stores
 */
export const LOG = logger.child({ store: 'default' })

const SETTINGS = 'settings'
const PLAYGROUND = 'playground'
const SET_VIEW = 'set_view'

/**
 * Gets the server endpoint from different sources (default and query strings)
 * @see [The URL.searchParams WebAPI]{@link https://developer.mozilla.org/en-US/docs/Web/API/URL/searchParams}
 * @returns {string} The server endpoint
 */
function getEndpoint () {
  let endpoint = `${location.hostname}:5000`

  const currentURL = new URL(document.location)
  const urlParameters = currentURL.searchParams
  const urlEndpoint = urlParameters.get('endpoint')

  if (urlEndpoint) {
    endpoint = urlEndpoint
  }

  return endpoint
}

/**
 * Gets all defined APIs that communicates with the Poire server
 * @param {external:socketIO/Socket} socket - The shared socket used to call pySatie
 * @returns {object} All APIs
 */
function getAPIs (socket) {
  const satieAPI = new SatieAPI(socket)
  const testAPI = new TestAPI(socket)
  const rendererAPI = new RendererAPI(socket)
  const nodeAPI = new NodeAPI(socket)
  const pluginAPI = new PluginAPI(socket)

  return {
    satieAPI,
    testAPI,
    rendererAPI,
    nodeAPI,
    pluginAPI
  }
}

const socket = io.connect(getEndpoint())

export const {
  satieAPI,
  testAPI,
  rendererAPI,
  nodeAPI,
  pluginAPI
} = getAPIs(socket)

const SatieContext = createContext()
const initialState = {
  view: SETTINGS,
  satieStatus: {
    initialized: false,
    connected: false
  },
  scConfig: {
    ip: '',
    port: '',
    sc_server: 'scsynth'
  },
  satieConfig: {
    no_channels: 0,
    aux_channels: 0,
    out_bus_index: 0,
    listeningFormat: ['stereoListener'],
    ambiOrders: []
  }
}

const reducer = (state, action) => {
  switch (action.type) {
    case SET_VIEW:
      return { ...state, view: action.view }
    case 'boot':
      return {
        ...state,
        satieStatus: {
          connected: true,
          initialized: true
        }
      }
    case 'disconnect':
      return {
        ...state,
        satieStatus: {
          connected: false,
          initialized: false
        }
      }
    default:
      throw new Error(`Unhandled action type ${action.type}`)
  }
}

export const SatieProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState)

  return (
    <SatieContext.Provider value={{ state, dispatch }}>
      {children}
    </SatieContext.Provider>
  )
}

SatieProvider.propTypes = {
  children: PropTypes.node.isRequired
}

export const useStore = () => useContext(SatieContext)
export { SETTINGS, PLAYGROUND }
