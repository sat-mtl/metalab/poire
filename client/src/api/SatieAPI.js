import API from '@api/API'

/**
 * @classdesc API that manages Satie
 * @memberof module:api
 * @extends module:api.API
 */
class SatieAPI extends API {
  /**
   * Requests the boot of SATIE
   * @async
   */
  boot () {
    return new Promise((resolve, reject) => {
      this.socket.emit('/satie/boot', (error, success) => {
        if (error) {
          reject(error)
        } else {
          resolve(success)
        }
      })
    })
  }

  /**
   * Requests the initialization of SATIE
   * @async
   */
  initialize () {
    return new Promise((resolve, reject) => {
      this.socket.emit('/satie/initialize', (error, success) => {
        if (error) {
          reject(error)
        } else {
          resolve(success)
        }
      })
    })
  }

  /**
   * Checks the connection status of SATIE
   * @async
   */
  isConnected () {
    return new Promise((resolve, reject) => {
      this.socket.emit('/satie/connected', (error, success) => {
        if (error) {
          reject(error)
        } else {
          resolve(success)
        }
      })
    })
  }

  /**
   * Request the disconnection of SATIE
   * @async
   */
  disconnect () {
    return new Promise((resolve, reject) => {
      this.socket.emit('/satie/disconnect', (error, success) => {
        if (error) {
          reject(error)
        } else {
          resolve(success)
        }
      })
    })
  }

  /**
   * Configure SATIE
   * @param {object} configuration - The JSON configuration of SATIE
   * @async
   */
  configure (configuration) {
    return new Promise((resolve, reject) => {
      this.socket.emit('/satie/configure', configuration, (error, success) => {
        if (error) {
          reject(error)
        } else {
          resolve(success)
        }
      })
    })
  }

  /**
   * Enable the SATIE heartbeat
   * @param {number} flag - A flag that enables SATIE (0: disable, 1: enable)
   * @async
   */

  enableHeartbeat (flag = 0) {
    return new Promise((resolve, reject) => {
      this.socket.emit('/satie/heartbeat/enable', flag, (error, success) => {
        if (error) {
          reject(error)
        } else {
          resolve(success)
        }
      })
    })
  }

  /**
   * Handle the pulse of the SATIE heartbeat
   * @param {Function} onPulseAction - Function triggered when the heartbeat is pulsed
   * @async
   */
  onHeartbeatPulse (onPulseAction = Function.prototype) {
    this.socket.on('/satie/heartbeat/pulse', (quidId) => {
      onPulseAction(quidId)
    })
  }
}

export default SatieAPI
