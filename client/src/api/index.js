/**
 * Socket provides the instance that is interacting with the server
 * @external socketIO/Socket
 * @see [Socket.io `Socket` client API]{@link https://socket.io/docs/v3/client-api/#Socket}
 */

/**
 * @module api
 * @desc Define all API routes used by the webapp
 */
