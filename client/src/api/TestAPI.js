import API from '@api/API'

/**
 * @classdesc API that tests Satie
 * @memberof module:api
 * @extends module:api.API
 */
class TestAPI extends API {
  /**
   * Request the test of a DAC channel
   * @param {number} dacChannel - DAC channel to test
   * @param {string} synthDef - Synth to use for the test
   * @param {object} extraArgs - Extra arguments for the test
   * @async
   */
  testDacChannel (dacChannel, synthDef, extraArgs) {
    return new Promise((resolve, reject) => {
      this.socket.emit('/satie/tester/testDacChannel', dacChannel, synthDef, extraArgs, (error, success) => {
        if (error) {
          reject(error)
        } else {
          resolve(success)
        }
      })
    })
  }

  /**
   * Stops the test of a DAC channel
   * @param {number} dacChannel - DAC channel to test
   * @async
   */
  stopDacChannel (dacChannel) {
    return new Promise((resolve, reject) => {
      this.socket.emit('/satie/tester/stopDacChannel', dacChannel, (error, success) => {
        if (error) {
          reject(error)
        } else {
          resolve(success)
        }
      })
    })
  }
}

export default TestAPI
