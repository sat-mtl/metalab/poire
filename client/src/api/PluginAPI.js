import API from '@api/API'

/**
 * @classdesc API that introspects all plugins
 * @memberof module:api
 * @extends module:api.API
 */
class PluginAPI extends API {
  /**
   * Handle the update of the plugin list
   * @param {Function} onUpdateAction - Function triggered when the list is updated
   * @async
   */
  onPluginUpdate (onUpdateAction) {
    this.socket.on('/satie/plugins/update', (quidId) => {
      onUpdateAction(quidId)
    })
  }

  /**
   * Query the instanciated audio sources
   * @async
   */
  queryAudioSources () {
    return new Promise((resolve, reject) => {
      this.socket.emit('/satie/audioSources/query', (error, success) => {
        if (error) {
          reject(error)
        } else {
          resolve(success)
        }
      })
    })
  }
}

export default PluginAPI
