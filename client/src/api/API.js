/**
 * @classdesc Mother class for all API classes
 * @memberof module:api
 */
class API {
  /**
   * Instantiates API with a socket
   * @param {external:socketIO/Socket} socket - The shared socket used to call pySatie
   */
  constructor (socket) {
    /** @property {external:socketIO/Socket} socket - Connected websocket with pySatie (Poire Server) */
    this.socket = socket
  }
}

export default API
