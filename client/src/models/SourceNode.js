
import '@styles/common/SourceNode.scss'

class SourceNode {
  constructor (nodeName, synthdefName) {
    this.nodeName = nodeName
    this.synthdefName = synthdefName
    this.azimuth = 0
    this.elevation = 0
    this.gain = -99
    this.delay = 1
    this.lphz = 15000
  }

  toMap () {
    return new Map([
      ['synthdefName', this.synthdefName],
      ['azimuth', this.azimuth],
      ['elevation', this.elevation],
      ['gain', this.gain],
      ['delay', this.delay],
      ['lphz', this.lphz]
    ])
  }

  static fromMap (nodeName, map) {
    const source = new SourceNode(nodeName, map.get('synthdefName'))

    source.azimuth = map.get('azimuth')
    source.elevation = map.get('elevation')
    source.gain = map.get('gain')
    source.delay = map.get('delay')
    source.lphz = map.get('lphz')

    return source
  }
}

export default SourceNode
