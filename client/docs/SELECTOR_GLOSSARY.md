<!-- AUTO GENERATED DOCUMENTATION: DO NOT MODIFY -->
# Glossary of CSS selectors
## components/bars
Should import the socket and create an event listener in back end
| Component name  | Description                                             | Css selector       |
| --------------- | ------------------------------------------------------- | ------------------ |
| MainVolumeInput | <p>Sets and displays the main volume of the server.</p> | `#MainVolumeInput` |

## components/bars.Navigation
| Component name | Description                                         | Css selector |
| -------------- | --------------------------------------------------- | ------------ |
| NavTab         | <p>A tab for the navigation</p>                     | `.NavTab`    |
| NavIcon        | <p>An icon for the tabs of the navigation panel</p> | `.NavIcon`   |

## components/bars
| Component name  | Description                                                             | Css selector       |
| --------------- | ----------------------------------------------------------------------- | ------------------ |
| Navigation      | <p>The navigation panel</p>                                             | `#Navigation`      |
| SatieStatus     | <p>All Satie statuses</p>                                               | `#SatieStatus`     |
| Sidebar         | <p>Poire's Sidebar. It contains the logo, navigation bar and status</p> | `#Sidebar`         |
| StopAudioButton | <p>A button that stops all audio devices</p>                            | `#StopAudioButton` |
| SystemUsage     | <p>Display the PySATIE server's memory and cpu usage.</p>               | `#SystemUsage`     |

## components/bars.SatieStatus
| Component name | Description                                       | Css selector  |
| -------------- | ------------------------------------------------- | ------------- |
| StatusInfo     | <p>Display a status given by the Satie server</p> | `.StatusInfo` |

## components/bars.SystemUsage
| Component name | Description                              | Css selector |
| -------------- | ---------------------------------------- | ------------ |
| Usage          | <p>Display the usage of some metrics</p> | `.Usage`     |

## components/common
| Component name | Description                                                                         | Css selector     |
| -------------- | ----------------------------------------------------------------------------------- | ---------------- |
| ListParameter  | <p>ListParameter a Parameter that uses a controlled dropdown menu for selection</p> | `.ListParameter` |
| Parameter      | <p>A Satie parameter</p>                                                            | `.Parameter`     |

## components/pages
| Component name | Description                | Css selector  |
| -------------- | -------------------------- | ------------- |
| Playground     | <p>The playground page</p> | `#Playground` |
| Settings       | <p>The Settings page</p>   | `#Settings`   |

## components/panels
| Component name | Description                                                                                       | Css selector      |
| -------------- | ------------------------------------------------------------------------------------------------- | ----------------- |
| CreateNode     | <p>The header of the playground. It submits the creation of a source.</p>                         | `.CreateNode`     |
| OutputChannels | <p>Displays an array of output channels available.</p>                                            | `.OutputChannels` |
| SatieConfig    | <p>Set SATIE's config. Receives only a function as props to change the view to the DACTester.</p> | `#SatieConfig`    |
| TesterConfig   | <p>TesterConfig is the header of the DACTester, where the parameters are set.</p>                 | `#TesterConfig`   |

## components/panels.OutChannelItem
| Component name | Description                                       | Css selector      |
| -------------- | ------------------------------------------------- | ----------------- |
| OutChannelItem | <p>OutChannelItem represents an audio channel</p> | `.OutChannelItem` |
