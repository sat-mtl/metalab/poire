const path = require('path')
const rootPath = path.resolve(__dirname, '..')

const HtmlPlugin = require('html-webpack-plugin')
const FaviconPlugin = require('favicons-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')

const { srcAliases } = require('./alias.config.js')

/**
 * Path to the JS entry point for the Poire web application
 * @see [Webpack entry configuration]{@link https://webpack.js.org/configuration/entry-context/#entry}
 * @member {string} jsEntry
 */
const jsEntry = path.resolve(rootPath, 'src/index.js')

/**
 * Path to the HTML entry point for the Poire web application
 * @see [Webpack HTML plugin]{@link https://webpack.js.org/plugins/html-webpack-plugin}
 * @member {string} htmlEntry
 */
const htmlEntry = path.resolve(rootPath, 'src/index.html')

/**
 * Path to the favicon picture
 * @see [Webpack favicon plugin]{@link https://github.com/jantimon/favicons-webpack-plugin}
 * @member {string} faviconEntry
 */
const faviconEntry = path.resolve(rootPath, 'assets/satie.png')

/**
 * Path to the output folder for the Poire bundle
 * @see [Webpack output configuration]{@link https://webpack.js.org/configuration/output/#outputfilename}
 * @member {string} entryFile
 */
const outputFolder = path.resolve(rootPath, 'dist')

/**
 * Environment variable defined when a specific NPM script is used
 * @see [NPM scripts documentation]{@link https://docs.npmjs.com/cli/v6/using-npm/scripts}
 * @member {string} npmScript
 */
const npmScript = process.env.npm_lifecycle_event

/**
 * Shared configuration for both production and development targets
 * @see [Webpack configuration]{@link https://webpack.js.org/configuration/#options}
 * @member {Object} commonConfig
 */
const commonConfig = {
  target: 'web',
  entry: jsEntry,
  output: {
    filename: '[name].bundle.js',
    path: outputFolder
  },
  resolve: {
    alias: srcAliases
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/,
      use: ['babel-loader']
    }, {
      test: /\.css$/i,
      use: ['style-loader', 'css-loader']
    }, {
      test: /\.s[ac]ss$/i,
      use: [
        'style-loader',
        {
          loader: 'css-loader',
          options: {
            sourceMap: true
          }
        }, {
          loader: 'sass-loader',
          options: {
            sourceMap: true
          }
        }]
    }, {
      test: /\.(woff|woff2|eot|ttf|otf)$/i,
      type: 'asset/inline'
    }, {
      test: /\.(png|jpg|gif)$/i,
      type: 'asset/resource'
    }, {
      test: /\.svg$/,
      issuer: /\.(js|md)x?$/,
      use: ['@svgr/webpack']
    }]
  },
  plugins: [
    new HtmlPlugin({
      template: htmlEntry
    }),
    new FaviconPlugin({
      logo: faviconEntry,
      mode: 'webapp',
      inject: true,
      favicons: {
        appName: 'Poire'
      }
    }),
    new CopyWebpackPlugin({
      patterns: [{
        from: 'assets',
        to: 'assets',
        context: 'node_modules/@sat-valorisation/ui-components'
      }]
    })
  ]
}

/**
 * Specific configuration used for development
 * @see [Webpack devtools configuration]{@link https://webpack.js.org/configuration/devtool/}
 * @member {Object} devConfig
 */
const devConfig = {
  mode: 'development',
  devtool: 'inline-source-map',
  devServer: {
    contentBase: outputFolder,
    host: '0.0.0.0'
  }
}

/**
 * Specific configuration used for production
 * @see [Webpack optimization configuration]{@link https://webpack.js.org/configuration/optimization/}
 * @member {Object} prodConfig
 */
const prodConfig = {
  mode: 'production',
  devtool: 'source-map'
}

if (npmScript === 'start') {
  module.exports = { ...commonConfig, ...devConfig }
} else if (npmScript === 'build') {
  module.exports = { ...commonConfig, ...prodConfig }
} else {
  module.exports = commonConfig
}
