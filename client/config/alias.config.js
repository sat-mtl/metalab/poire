const path = require('path')

const rootPath = path.resolve(__dirname, '..')

/**
 * The aliases allow the use of shortcuts in the Poire imports
 * @see [Webpack aliases]{@link https://webpack.js.org/configuration/resolve/#resolvealias}
 * @member {Object} srcAlias
 */
const srcAliases = {
  '~': `${rootPath}`,
  '@api': `${rootPath}/src/api`,
  '@assets': `${rootPath}/assets`,
  '@components': `${rootPath}/src/components`,
  '@models': `${rootPath}/src/models`,
  '@stores': `${rootPath}/src/stores`,
  '@styles': `${rootPath}/src/styles`,
  '@utils': `${rootPath}/src/utils`
}

/**
 * Same aliases as them used for the source, but they are adapted to Jest
 * @see [Jest moduleNameMapper configuration]{@link https://jestjs.io/docs/configuration#modulenamemapper-objectstring-string--arraystring}
 * @member {Object} testAlias
 */
const testAliases = {
  '^~(.*)': '<rootDir>/$1',
  '^@api(.*)': '<rootDir>/src/api$1',
  '^@assets(.*)': '<rootDir>/assets$1',
  '^@components(.*)': '<rootDir>/src/components$1',
  '^@models(.*)': '<rootDir>/src/models$1',
  '^@stores(.*)': '<rootDir>/src/stores$1',
  '^@styles(.*)': '<rootDir>/src/styles$1',
  '^@utils(.*)': '<rootDir>/src/utils$1'
}

module.exports = {
  srcAliases,
  testAliases
}
