/* global describe it expect jest beforeEach */

import populateStores from '~/src/populateStores.js'
import { MockedSocket } from '~/test/fixtures.js'

const OUTPUT_DB = 1

describe('RendererStore', () => {
  let socketStore, rendererStore

  beforeEach(() => {
    ({ socketStore, rendererStore } = populateStores())
  })

  describe('handleSocketChange', () => {
    it('should handle the socket change when the active socket is set', () => {
      rendererStore.handleSocketChange = jest.fn()
      socketStore.setActiveSocket(new MockedSocket())
      expect(rendererStore.handleSocketChange).toHaveBeenCalled()
    })

    it('should configure the RendererApi when the socket is active', () => {
      expect(rendererStore.rendererAPI).toEqual(null)
      rendererStore.handleSocketChange(new MockedSocket())
      expect(rendererStore.rendererAPI).not.toEqual(null)
    })
  })

  describe('applyOutputDB', () => {
    beforeEach(() => {
      socketStore.setActiveSocket(new MockedSocket())
      rendererStore.rendererAPI.setOutputDB = jest.fn()
    })

    it('should request a new outputDB value', () => {
      rendererStore.applyOutputDB(OUTPUT_DB)
      expect(rendererStore.rendererAPI.setOutputDB).toHaveBeenCalledWith(OUTPUT_DB)
    })
  })

  describe('setOutputDB', () => {
    let socket, onAction

    beforeEach(() => {
      socket = new MockedSocket()

      socket.on = (msg, action) => {
        if (msg === '/satie/renderer/outputDB/changed') {
          onAction = action
        }
      }

      socketStore.setActiveSocket(socket)
      rendererStore.setOutputDB = jest.fn()
    })

    it('should set the output DB when the outputDB is changed', () => {
      onAction(OUTPUT_DB)
      expect(rendererStore.setOutputDB).toHaveBeenCalledWith(OUTPUT_DB)
    })

    it('should set the output DB when the outputDB is applied', () => {
      rendererStore.applyOutputDB(OUTPUT_DB)
      expect(rendererStore.setOutputDB).toHaveBeenCalledWith(OUTPUT_DB)
    })
  })
})
