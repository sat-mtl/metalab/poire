/* global describe it expect jest beforeEach */

import populateStores from '~/src/populateStores.js'
import { MockedSocket } from '~/test/fixtures.js'

describe('SocketStore', () => {
  let socketStore

  beforeEach(() => {
    ({ socketStore } = populateStores())
  })

  describe('endpoint', () => {
    it('should be the default port with the default host', () => {
      expect(socketStore.endpoint).toEqual('localhost:5000')
    })

    it('should be redefined by the current URL parameters', () => {
      Object.defineProperty(window, 'location', {
        writable: true,
        value: new URL('http://localhost:8080?endpoint=test:1111')
      })

      expect(socketStore.endpoint).toEqual('test:1111')
    })
  })

  describe('applyConnection', () => {
    it('should configure the socket reconnection failure', () => {
      const mockedSocket = new MockedSocket()
      socketStore.applyConnection(mockedSocket)
      expect(mockedSocket.io.on).toHaveBeenNthCalledWith(1, 'reconnect_failed', expect.any(Function))
    })

    it('should configure the socket connection', () => {
      const mockedSocket = new MockedSocket()
      socketStore.applyConnection(mockedSocket)
      expect(mockedSocket.on).toHaveBeenNthCalledWith(1, 'connect', expect.any(Function))
    })
  })

  describe('handleSocketConnection', () => {
    it('should configure the socket disconnection', () => {
      const mockedSocket = new MockedSocket()
      socketStore.handleSocketConnection(mockedSocket)
      expect(mockedSocket.on).toHaveBeenCalledWith('disconnect', expect.any(Function))
    })

    it('should set the connected socket as the active socket', () => {
      socketStore.setActiveSocket = jest.fn()
      const mockedSocket = new MockedSocket()
      socketStore.handleSocketConnection(mockedSocket)
      expect(socketStore.setActiveSocket).toHaveBeenCalledWith(mockedSocket)
    })
  })

  describe('handleSocketDisconnection', () => {
    it('should reset the active socket', () => {
      socketStore.setActiveSocket = jest.fn()
      socketStore.handleSocketDisconnection()
      expect(socketStore.setActiveSocket).toHaveBeenCalledWith(null)
    })
  })
})
