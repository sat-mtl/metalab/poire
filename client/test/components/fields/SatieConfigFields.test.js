/* global jest beforeEach afterEach describe it expect */

import { cleanup, screen } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import userEvent from '@testing-library/user-event'

import React from 'react'

import populateStores from '~/src/populateStores.js'
import { AVAILABLE_LISTENING_FORMATS } from '@stores/ConfigStore.js'

import { renderWithContext } from '~/test/utils/testUtils'

import {
  ServerNameInput,
  SupernovaSwitch,
  ListeningFormatSelect,
  OutBusIndexInput,
  NumAudioAuxInput,
  AmbiOrdersInput,
  MinOutputBusChannelsInput
} from '@components/fields/SatieConfigFields'

afterEach(cleanup)

let stores

beforeEach(() => {
  stores = populateStores()
  stores.configStore.updateSatieConfig = jest.fn()
})

describe('<ServerNameInput />', () => {
  it('should render the field with its title', () => {
    renderWithContext(<ServerNameInput />, { stores })
    expect(screen.getByText(/SuperCollider Server/i)).toBeInTheDocument()
  })

  it('should update the configuration when it is blurred', () => {
    renderWithContext(<ServerNameInput />, { stores })
    const $input = screen.getByRole('textbox')
    userEvent.type($input, 'TEST')

    expect(stores.configStore.updateSatieConfig).not.toHaveBeenCalled()
    $input.blur()

    expect(stores.configStore.updateSatieConfig).toHaveBeenCalledWith({
      server: {
        name: 'TEST',
        supernova: stores.configStore.satieConfig.server.supernova
      }
    })
  })
})

describe('<SupernovaSwitch />', () => {
  it('should render the field with its title', () => {
    renderWithContext(<SupernovaSwitch />, { stores })
    expect(screen.getByText(/Supernova Server/i)).toBeInTheDocument()
  })

  it('should update the configuration when it is clicked', () => {
    renderWithContext(<SupernovaSwitch />, { stores })
    const $input = screen.getByRole('button')
    userEvent.click($input)

    expect(stores.configStore.updateSatieConfig).toHaveBeenCalledWith({
      server: {
        name: stores.configStore.satieConfig.server.name,
        supernova: !stores.configStore.satieConfig.server.supernova
      }
    })
  })
})

describe('<ListeningFormatSelect />', () => {
  it('should render the field with its title', () => {
    renderWithContext(<ListeningFormatSelect />, { stores })
    expect(screen.getByText(/Listening format/i)).toBeInTheDocument()
  })

  it('should select the configured format by default', () => {
    renderWithContext(<ListeningFormatSelect />, { stores })
    expect(screen.getByText(stores.configStore.currentListeningFormat.label)).toBeInTheDocument()
  })

  it('should update the configuration when it is clicked', () => {
    renderWithContext(<ListeningFormatSelect />, { stores })
    const domeOpt = AVAILABLE_LISTENING_FORMATS.find(opt => opt.value === 'domeVBAP')

    userEvent.click(screen.getByRole('button'))
    userEvent.click(screen.getByText(domeOpt.label))

    expect(stores.configStore.updateSatieConfig).toHaveBeenCalledWith({
      listeningFormat: [domeOpt.value]
    })
  })
})

describe('<OutBusIndexInput />', () => {
  it('should render the field with its title', () => {
    renderWithContext(<OutBusIndexInput />, { stores })
    expect(screen.getByText('Output channel offset')).toBeInTheDocument()
  })

  it('should update the configuration when it is blurred', () => {
    const { container } = renderWithContext(<OutBusIndexInput />, { stores })
    const $input = container.querySelector('.InputNumberField')
    userEvent.type($input, '17')

    expect(stores.configStore.updateSatieConfig).not.toHaveBeenCalled()
    $input.blur()

    expect(stores.configStore.updateSatieConfig).toHaveBeenCalledWith({
      outBusIndex: [17]
    })
  })
})

describe('<NumAudioAuxInput />', () => {
  it('should render the field with its title', () => {
    renderWithContext(<NumAudioAuxInput />, { stores })
    expect(screen.getByText('Number of auxiliary channels')).toBeInTheDocument()
  })

  it('should update the configuration when it is blurred', () => {
    const { container } = renderWithContext(<NumAudioAuxInput />, { stores })
    const $input = container.querySelector('.InputNumberField')
    userEvent.type($input, '{backspace}17')

    expect(stores.configStore.updateSatieConfig).not.toHaveBeenCalled()
    $input.blur()

    expect(stores.configStore.updateSatieConfig).toHaveBeenCalledWith({
      numAudioAux: 17
    })
  })
})

describe('<AmbiOrdersInput />', () => {
  it('should render the field with its title', () => {
    renderWithContext(<AmbiOrdersInput />, { stores })
    expect(screen.getByText('Ambisonic Orders')).toBeInTheDocument()
  })

  it('should update the configuration when it is blurred', () => {
    const { container } = renderWithContext(<AmbiOrdersInput />, { stores })
    const $input = container.querySelector('.InputNumberField')
    userEvent.type($input, '{backspace}3')

    expect(stores.configStore.updateSatieConfig).not.toHaveBeenCalled()
    $input.blur()

    expect(stores.configStore.updateSatieConfig).toHaveBeenCalledWith({
      ambiOrders: [3]
    })
  })
})

describe('<MinOutputBusChannelsInput />', () => {
  it('should render the field with its title', () => {
    renderWithContext(<MinOutputBusChannelsInput />, { stores })
    expect(screen.getByText('Number of server outputs channels')).toBeInTheDocument()
  })

  it('should update the configuration when it is blurred', () => {
    const { container } = renderWithContext(<MinOutputBusChannelsInput />, { stores })
    const $input = container.querySelector('.InputNumberField')
    userEvent.type($input, '{backspace}17')

    expect(stores.configStore.updateSatieConfig).not.toHaveBeenCalled()
    $input.blur()

    expect(stores.configStore.updateSatieConfig).toHaveBeenCalledWith({
      minOutputBusChannels: 17
    })
  })
})
