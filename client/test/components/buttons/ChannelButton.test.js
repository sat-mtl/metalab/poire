/* global jest beforeEach afterEach describe it expect */

import { cleanup, screen } from '@testing-library/react'
import { renderWithContext } from '~/test/utils/testUtils'
import '@testing-library/jest-dom/extend-expect'
import userEvent from '@testing-library/user-event'

import React from 'react'

import populateStores from '~/src/populateStores.js'
import ChannelButton from '@components/buttons/ChannelButton'

afterEach(cleanup)

describe('<ChannelButton />', () => {
  let stores
  const CHANNEL_NUM = 1

  beforeEach(() => {
    stores = populateStores()

    stores.testerStore.applyDacTest = jest.fn()
  })

  it('render without crashing', () => {
    renderWithContext(<ChannelButton />)
  })

  it('should apply DAC tests on user click', () => {
    renderWithContext(<ChannelButton channelNum={CHANNEL_NUM} />, { stores })
    userEvent.click(screen.getByRole('button'))
    expect(stores.testerStore.applyDacTest).toHaveBeenCalledWith(CHANNEL_NUM)
  })

  it('should be outlined by default', () => {
    renderWithContext(<ChannelButton channelNum={CHANNEL_NUM} />, { stores })
    expect(screen.getByRole('button')).toHaveClass('button-primary-outlined')
  })

  it('should not be outlined when the associated channel is tested', () => {
    renderWithContext(<ChannelButton channelNum={CHANNEL_NUM} />, { stores })
    stores.testerStore.setTestingChannel(CHANNEL_NUM)
    expect(screen.getByRole('button')).not.toHaveClass('button-primary-outlined')
  })
})
