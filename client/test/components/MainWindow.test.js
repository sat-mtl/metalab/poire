/* global afterEach beforeEach describe it expect */

import { cleanup } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'

import React from 'react'

import { renderWithContext } from '~/test/utils/testUtils'
import { MainWindow } from '@components/App'

import populateStores from '~/src/populateStores.js'
import PageStore from '@stores/PageStore'
import PageEnum from '@models/PageEnum'

describe('<MainWindow />', () => {
  let stores

  afterEach(cleanup)

  beforeEach(() => {
    stores = {
      ...populateStores(),
      pageStore: new PageStore()
    }
  })

  it('renders without crashing', () => {
    renderWithContext(<MainWindow />)
  })

  it('should render the Settings page by default', () => {
    const { container } = renderWithContext(<MainWindow />, { stores })
    expect(container.querySelector('#Settings')).not.toBeNull()
    expect(container.querySelector('#DACTester')).toBeNull()
    expect(container.querySelector('#Playground')).toBeNull()
  })

  it('should render the Playground page if it is selected', () => {
    const { container } = renderWithContext(<MainWindow />, { stores })
    stores.pageStore.setActivePage(PageEnum.PLAYGROUND)
    expect(container.querySelector('#Settings')).toBeNull()
    expect(container.querySelector('#DACTester')).toBeNull()
    expect(container.querySelector('#Playground')).not.toBeNull()
  })

  it('should render the DAC Tester page if it is selected', () => {
    const { container } = renderWithContext(<MainWindow />, { stores })
    stores.pageStore.setActivePage(PageEnum.DAC_TESTER)
    expect(container.querySelector('#Settings')).toBeNull()
    expect(container.querySelector('#DACTester')).not.toBeNull()
    expect(container.querySelector('#Playground')).toBeNull()
  })
})
