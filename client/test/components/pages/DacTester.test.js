/* global jest beforeEach afterEach describe it expect */

import { cleanup, screen, within } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import userEvent from '@testing-library/user-event'

import React from 'react'

import populateStores from '~/src/populateStores.js'

import { renderWithContext } from '~/test/utils/testUtils'
import DacTester from '@components/pages/DacTester'

afterEach(cleanup)

describe('<DacTester />', () => {
  let stores

  beforeEach(() => {
    stores = populateStores()
    stores.testerStore.applyDacCycle = jest.fn()
  })

  it('render without crashing', () => {
    renderWithContext(<DacTester />, { stores })
  })

  describe('Common Settings', () => {
    it('should render all common settings', () => {
      const { container } = renderWithContext(<DacTester />, { stores })
      const Common = container.querySelector('#CommonSettingsSection')
      expect(within(Common).getAllByRole('group').length).toEqual(2)
    })
  })

  describe('Extra Settings', () => {
    it('should not render the ExtraSettingsSection by default', () => {
      const { container } = renderWithContext(<DacTester />, { stores })
      expect(container.querySelector('#ExtraSettingsSection')).toBeFalsy()
    })

    it('should render the extra setting section with the frequency field when the sine signal is selected', () => {
      const { container } = renderWithContext(<DacTester />, { stores })
      stores.testerStore.updateTesterConfig({ testSynth: 'sine' })
      const Extra = container.querySelector('#ExtraSettingsSection')
      expect(within(Extra).getAllByRole('group').length).toEqual(1)
    })
  })

  it('should trigger the test cycle when the cycle button is clicked', () => {
    renderWithContext(<DacTester />, { stores })
    userEvent.click(screen.getByText('Loop tests'))
    expect(stores.testerStore.applyDacCycle).toHaveBeenCalled()
  })

  describe('OutputChannels', () => {
    it('should display as many buttons as there are configured output channels', () => {
      const { container } = renderWithContext(<DacTester />, { stores })
      const Panel = container.querySelector('#OutputChannelsPanel')
      expect(within(Panel).getAllByRole('button').length).toEqual(stores.configStore.satieConfig.minOutputBusChannels)
    })
  })
})
