/* global afterEach describe it expect */

import { cleanup } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'

import React from 'react'

import { renderWithContext } from '~/test/utils/testUtils'
import Sidebar from '@components/bars/Sidebar'

afterEach(cleanup)

describe('Sidebar', () => {
  it('renders without crashing', () => {
    renderWithContext(<Sidebar />)
  })

  it('Renders sidebar with Settings selected', () => {
    const { getByText } = renderWithContext(<Sidebar />)
    const settingsTab = getByText(/Settings/i)
    const playgroundTab = getByText(/Playground/i)
    expect(settingsTab).toBeInTheDocument()
    expect(playgroundTab).toBeInTheDocument()
  })

  it('should not display volume slider or stop button if not connected', () => {
    const { queryByText } = renderWithContext(<Sidebar />)
    const stopButton = queryByText(/stop/i)
    const volumeSlider = queryByText(/volume/i)
    expect(stopButton).toBeNull()
    expect(volumeSlider).toBeNull()
  })
})
